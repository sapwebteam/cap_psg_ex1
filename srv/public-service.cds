using db from '../db/data-model';

using {NorthWind as external} from './external/NorthWind.csn';

service PublicService {
    entity Projects as projection on db.Projects;
    entity ProjectsInt as projection on db.ProjectsInt;
    entity Votes as projection on db.Votes;
    entity ProductInt as projection on db.ProductInt;

    @readonly
    entity Products as projection on external.Products {
        key ID, Name, Description, ReleaseDate, DiscontinuedDate, Rating, Price
    };


}

