const cds = require('@sap/cds');

module.exports = cds.service.impl(async function() {
    const { Products } = this.entities;
    const service = await cds.connect.to('externalService'); //Nome del modello del package.json
    
    this.on('READ', Products, request => {
        return service.tx(request).run(request.query);
    });

    this.after('READ', 'Projects', each => {
        try {
            console.log(JSON.stringify(each));
            let CQN = {INSERT:{
                        into: { ref: ['ProjectsInt'] },
                        columns: [ 'name', 'language', 'repository'],
                        rows: [
                            [ each.name, each.language, each.repository ]
                        ]
                    }}
            cds.run (CQN);    
        } catch (error) {
            console.log(error);
        }  
    })

    /*
    this.after('READ', 'Products', ret => {
        try {
            let CQN = {INSERT:{
                        into: { ref: ['ProjectsInt'] },
                        columns: [ 'name', 'language', 'repository'],
                        rows: [
                            [ ret.name, ret.language, ret.repository ]
                        ]
                    }}
            cds.run (CQN);    
        } catch (error) {
            console.log(error);
        }  
    })

    this.after('READ', Products, request => {
            //if(request.Name === 'Milk'){
            //var aRet = await cds.run (SELECT.from(Products)),//service.tx(request).run(request.query),
                aInput = [];

            aRet.forEach(await function(currVal) {
                var aCurrObj = [];
                aCurrObj.push(currVal.ID);
                aCurrObj.push(currVal.Description);
                aInput.push(aCurrObj);
            })

            let CQN = {INSERT:{
                        into: { ref: ['MariaTable'] },
                        columns: [ 'ProductID', 'Details'],
                        rows: aInput
                    }}
            cds.run (CQN);
    }

*/



});
